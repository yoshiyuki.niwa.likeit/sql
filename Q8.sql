SELECT
	item_id,
	item_name,
	item_price,
	category_name
FROM
	item_category ic
INNER JOIN
	item i1
ON
	ic.category_id = i1.category_id;
