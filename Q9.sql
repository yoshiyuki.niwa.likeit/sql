SELECT
	ic.category_name,
	SUM(i1.item_price)AS total_price
FROM
	item_category ic
INNER JOIN
	item i1
ON
	ic.category_id = i1.category_id
GROUP BY
	ic.category_name;